import en from './locales/en_US.js'
import ru from './locales/ru_RU.js'

module.exports = {
    /*
     ** Headers of the page
     */
    head: {
        title: 'TRANSAVIA',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
            { hid: 'description', name: 'description', content: 'Nuxt.js project' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap' },
        ],
        script: [
            {
                src: '//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit' , async: true
            },
        ],
    },

    server: {
        port: 8080, // default: 3000
        host: 'localhost' // default: localhost
    },

    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/proxy',
        'bootstrap-vue/nuxt',
        ['cookie-universal-nuxt', { alias: 'cookiz' }],
        ['nuxt-i18n', {
            vueI18n: {
                fallbackLocale: 'ru',
                messages: {
                    en: en,
                    ru: ru
                }
            },
            loadLanguagesAsync: true,
            lazy: true,
            defaultLocale: 'ru',}],
        '@nuxtjs/sitemap'
    ],
    axios: {
        baseURL: `https://avia.test.transavia.kz/api/v1/avia/`,
        credentials: false,
        //proxy: true
    },

    proxy: {
        '/api': {target: 'https://avia.test.transavia.kz/api/v1/avia/', pathRewrite: {
                '^/api' : '/'
            }},
        '/api2': {
            target: 'https://adminb2c.transavia.kz/',
            changeOrigin: true
        }
    },

    i18n: {},
    sitemap: {
        hostname: 'https://nuxt.avia.transavia.kz/',
        gzip: true,
    },

    plugins: [
        '~/plugins/globals',
        '~/plugins/axios',
        "~/plugins/eventBus",
        '~/plugins/filters.js',
        { src: '~/plugins/vue-notifications', ssr: false },
        { src: '~/plugins/vue-airbnb-datepicker.js', ssr: false },

    ],

    /*
     ** Customize the progress bar color
     */
    //loading: 'components/elements/loading.vue',
    loadingIndicator: {
        name: 'circle',
        color: '#3B8070',
        background: 'white'
    },
    /*
     ** Global CSS
     */
    css: [
        '@/node_modules/font-awesome/css/font-awesome.min.css',
        '@/assets/styles/app.sass'
    ],
    /*
     ** Build configuration
     */
    build: {
        //vendor: ['~/plugins/vue-notifications'],
        /*
         ** Run ESLint on save
         */
        extend(config, { isDev, isClient }) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    //loader: 'eslint-loader',
                    exclude: /(node_modules)/
                });
            }
        }
    }
};
