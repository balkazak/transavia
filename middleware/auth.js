export default function ({ store, redirect, app, route  }) {
    //get balance
    if(!app.$cookiz.get('token')) {
        return redirect('/login')
    } else {
        store.dispatch('cabinet/loadAccount');
    }
}
