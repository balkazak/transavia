let authURL     = 'https://auth.test.transavia.kz/';
let baseUrl = 'https://avia.test.transavia.kz/api/v1/';
import Cookies from 'js-cookie'


const state = () => ({
    authToken: null,
    account: null,
    passengers: null,
    passenger: null
});

const getters = {
    getAccount (state) {
        return state.account
    },
    getAllPassengers (state) {
        return state.passengers
    },
    getPassenger (state) {
        return state.passenger
    },
}

const mutations = {
    setAuthToken(state, authToken = null) {
        state.authToken = authToken;
    },
    setAccount(state, account = null) {
        state.account = account
    },
    setAllPassengers(state, passengers = null) {
        state.passengers = passengers
    },
    setPassenger(state, passengers = null) {
        state.passengers = passengers
    },
};

const actions = {
    setAuthToken: ({ commit }, authToken = null) => {
        Cookies.set('token', authToken);
        commit('setAuthToken', authToken);
    },
    async loadAccount({ commit, dispatch }) {
        const res = await this.$axios.$get(`${authURL}partners/cabinet/`);
        commit('setAccount', res);
        return dispatch('notifications/handle', res, { root: true });
    },
    async refreshToken({ commit, dispatch }, token = null) {
        const res = await this.$axios.$post(`${authURL}auth/token/refresh`, {'token': token });
        commit('setAuthToken', res.token);
        return dispatch('notifications/handle', res, { root: true });
    },
    async getPassengers({ commit, dispatch }, data) {
        const res = await this.$axios.get(`${baseUrl}passenger?company_or_group=company`, data, {
            // params: {
            //     page: data.pageNumber
            // }
        });
        if (res.hasOwnProperty('data') && res.data.hasOwnProperty('results')) {
            commit('setAllPassengers', res.data.results);
        }
        return dispatch('notifications/handle', res, { root: true });
    },
    async updatePassenger({ commit, dispatch }, data) {
        const res = await this.$axios.put(`${baseUrl}passenger/${data.id}`, data, {
            // params: {
            //     page: data.pageNumber
            // }
        });
        if (res.hasOwnProperty('data') && res.data.hasOwnProperty('results')) {
            commit('setPassenger', res.data.results);
        }
        return dispatch('notifications/handle', res, { root: true });
    },
    async deletePassenger({ commit, dispatch }, data) {
        const res = await this.$axios.delete(`${baseUrl}passenger/${data}`, data, {
            // params: {
            //     page: data.pageNumber
            // }
        });
        if (res.hasOwnProperty('data') && res.data.hasOwnProperty('results')) {
            commit('setPassenger', res.data.results);
        }
        return dispatch('notifications/handle', res, { root: true });
    },
};

export default {
    state,
    actions,
    mutations,
    getters,
};
