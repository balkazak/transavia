import https from 'https';
import Cookies from 'js-cookie'

const newsHost = 'https://adminb2c.transavia.kz/public/api/articles/';
let authURL    = 'https://auth.test.transavia.kz/';

const state = () => ({
  news: []
});

const getters = {
  news: state => state.news
};

const mutations = {
  SET_NEWS(state, news) {
    state.news = news;
  }
};

const actions = {
  async nuxtServerInit({ commit, dispatch }) {
      if ((Cookies.get('token'))) {
          await dispatch('cabinet/refreshToken', Cookies.get('token'));
          await dispatch('cabinet/loadAccount');
      }
      else {
          this.$router.push('/login')
      }
      const agent = new https.Agent({
      rejectUnauthorized: false
    });

    // News запрос
    // const { data } = await this.$axios.get(newsHost, { httpsAgent: agent });
    commit('SET_NEWS', null);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
