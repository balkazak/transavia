import Vue from 'vue'
import AirbnbStyleDatepicker from 'vue-airbnb-style-datepicker'
import 'vue-airbnb-style-datepicker/dist/vue-airbnb-style-datepicker.css'

const datepickerOptions = {
    dateLabelFormat: 'dddd, MMMM D, YYYY',
    days: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
    daysShort: ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'],
    monthNames: [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь',
    ],
    // days: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
    // daysShort: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
    // monthNames: [
    //     'January',
    //     'February',
    //     'March',
    //     'April',
    //     'May',
    //     'June',
    //     'July',
    //     'August',
    //     'September',
    //     'October',
    //     'November',
    //     'December',
    // ],
    colors: {
        selected: '#4b77be',
        inRange: '#4b77be80',
        selectedText: '#fff',
        text: '#757575',
        inRangeBorder: '#4b77be80',
        disabled: '#75757582',
        hoveredInRange: '#4b77be47'
    },
    texts: {
        apply: 'Обратный билет не нужен',
        cancel: ' ',
    },
}

Vue.use(AirbnbStyleDatepicker, datepickerOptions)
