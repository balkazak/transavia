const state = () => ({
  loader: false
});

const getters = {
  getLoader: state => state.loader
};

const mutations = {
  SET_LOADER(state, loader = null) {
    state.loader = loader;
  }
};

const actions = {
  SET_LOADER: ({ commit }, loader = null) => {
    commit('SET_LOADER', loader);
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
