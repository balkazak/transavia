
export default async function ({ $axios, store, app }) {

  $axios.onRequest(config => {
      config.headers.Authorization = 'TRV ' + app.$cookiz.get('token');
  })

    $axios.interceptors.response.use((response) => {
        return response;
    }, function (error) {
        // Do something with response error
        let err = {};
        if (error.response.data.hasOwnProperty('rs') && error.response.data.hasOwnProperty('sts') && error.response.data.sts === 'error') {
            err.type = error.response.data.rs.ty;
            err.msg = error.response.data.rs.rsns[0];
            err.sts = 'warning';
        } else if (error.response.status === 401) {
            store.dispatch('cabinet/refreshToken', app.$cookiz.get('token'))
        } else if (error.response.status === 400) {
            app.router.push('/login')
        }
        else if (error.response.data.hasOwnProperty('detail') && error.response.data.detail === 'Expired') {
            app.router.push('/login')
        }
        else {
            err.type = 'Произошла ошибка';
            err.msg = 'Попробуйте еще раз';
        }
        store.commit('notifications/setError', err)
        //return error.response
        return Promise.reject(error.response);
    });
}

