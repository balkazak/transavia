let authURL     = 'https://auth.test.transavia.kz/';
let baseUrl = 'https://avia.test.transavia.kz/api/v1/avia/';
import Cookies from 'js-cookie'


const state = () => ({
    authToken: null,
    account: null,
    accountFee: null,
    tickets: null,
    rules: null,
});

const getters = {
    getAccount (state) {
        return state.account
    },
    getTickets (state) {
        return state.tickets
    },
    getRules (state) {
        return state.rules
    },
}

const mutations = {
    setAuthToken(state, authToken = null) {
        state.authToken = authToken;
    },
    setAccount(state, account = null) {
        state.account = account
    },
    setAccountFee(state, accountFee = null) {
        state.accountFee = accountFee
    },
    setTickets(state, tickets = null) {
        state.tickets = tickets
    },
    setRules(state, rules = null) {
        state.rules = rules
    },
};

const actions = {
    setAuthToken: ({ commit }, authToken = null) => {
        Cookies.set('token', authToken);
        commit('setAuthToken', authToken);
    },
    async sendToMail({ commit, dispatch }, data = null) {
        const res = await this.$axios.$post(`${baseUrl}ticket_application`, data);
        return dispatch('notifications/handle', res, { root: true });
    },
    async loadAccount({ commit, dispatch }) {
        const res = await this.$axios.$get(`${authURL}partners/cabinet/`);
        commit('setAccount', res);
        return dispatch('notifications/handle', res, { root: true });
    },
    async refreshToken({ commit, dispatch }, token = null) {
        const res = await this.$axios.$post(`${authURL}auth/token/refresh`, {'token': token });
        commit('setAuthToken', res.token);
        return dispatch('notifications/handle', res, { root: true });
    },
    async loadFee({ commit, dispatch }, data = null) {
        const res = await this.$axios.$put(`${authURL}partners/cabinet/`, data);
        commit('setAccountFee', res);
        return dispatch('notifications/handle', res, { root: true });
    },
    async loadTickets({ commit, dispatch }, data) {
        const res = await this.$axios.$post(`${baseUrl}my_tickets`, data, {
            params: {
                page: data.pageNumber
            }
        });
        if (res.hasOwnProperty('rs') && res.rs.hasOwnProperty('books')) {
            commit('setTickets', res.rs.books);
        }
        return dispatch('notifications/handle', res, { root: true });
    },
    setRules: ({ commit }, rules) => {
        commit('setRules', rules);
    },
};

export default {
    state,
    actions,
    mutations,
    getters,
};
