import Vue from 'vue'
import moment from 'moment'

Vue.filter( 'dateFormat', function (value, format = '', row = false) {
  if (!row) {
    return moment(value).format(format)
  }
  // Split
  var arr = value.split(':')
  return (arr[0] + ' hours, ' + parseInt(arr[1]) + ' mins.')
});

Vue.filter('titleFormat', function (str) {
  return str.split('_').map(el => el.charAt(0).toUpperCase() + el.slice(1)).join(' ')
})

Vue.filter('rulesFormat', function (str) {
    // return str.replaceAll('.', '.\n\r')
    str = str.replace('.................................................', '\n\r\n\r');
    str = str.replaceAll('.', '.\n\r');
    str = str.replaceAll('--------------------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-------------------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('------------------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('----------------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('---------------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('--------------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-------------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('------------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('----------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('---------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('--------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('------------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('----------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('---------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('--------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('------------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('----------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('---------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('--------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('------------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('----------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('---------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('--------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('------------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('----------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('---------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('--------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('------------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('----------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('---------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('--------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('------------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------------------------------', '\n\r\n\r');
    str = str.replaceAll('----------------------------------------', '\n\r\n\r');
    str = str.replaceAll('---------------------------------------', '\n\r\n\r');
    str = str.replaceAll('--------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-------------------------------------', '\n\r\n\r');
    str = str.replaceAll('------------------------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------------------------', '\n\r\n\r');
    str = str.replaceAll('----------------------------------', '\n\r\n\r');
    str = str.replaceAll('---------------------------------', '\n\r\n\r');
    str = str.replaceAll('--------------------------------', '\n\r\n\r');
    str = str.replaceAll('-------------------------------', '\n\r\n\r');
    str = str.replaceAll('------------------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------------------', '\n\r\n\r');
    str = str.replaceAll('----------------------------', '\n\r\n\r');
    str = str.replaceAll('---------------------------', '\n\r\n\r');
    str = str.replaceAll('--------------------------', '\n\r\n\r');
    str = str.replaceAll('-------------------------', '\n\r\n\r');
    str = str.replaceAll('------------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------------', '\n\r\n\r');
    str = str.replaceAll('----------------------', '\n\r\n\r');
    str = str.replaceAll('---------------------', '\n\r\n\r');
    str = str.replaceAll('--------------------', '\n\r');
    str = str.replaceAll('-------------------', '\n\r\n\r');
    str = str.replaceAll('------------------', '\n\r\n\r');
    str = str.replaceAll('-----------------', '\n\r\n\r');
    str = str.replaceAll('----------------', '\n\r\n\r');
    str = str.replaceAll('---------------', '\n\r\n\r');
    str = str.replaceAll('--------------', '\n\r\n\r');
    str = str.replaceAll('-------------', '\n\r\n\r');
    str = str.replaceAll('------------', '\n\r\n\r');
    str = str.replaceAll('-----------', '\n\r\n\r');
    str = str.replaceAll('----------', '\n\r\n\r');
    str = str.replaceAll('---------', '\n\r\n\r');
    str = str.replaceAll('--------', '\n\r\n\r');
    str = str.replaceAll('-------', '\n\r\n\r');
    str = str.replaceAll('------', '\n\r\n\r');
    str = str.replaceAll('-----', '\n\r\n\r');
    str = str.replaceAll('----', '\n\r\n\r');
    str = str.replaceAll('---', '\n\r\n\r');
    str = str.replaceAll('--', '\n\r\n\r');
    // let x = null;
    // for (let i = 0; i < 500; i++) {
    //     if (str[i] === '.') {
    //         console.log('dot here');
    //         x = str.substring(0, i) + "MAHAMBET" + str.substring(i, 500);
    //     }
    // }
    // str = x;
    return str;

})

Vue.filter('truncate',function (source, size = 20) {
  if (typeof  source === 'undefined') {
    return ''
  }
  return source.length > size ? source.slice(0, size - 1) + "…" : source;
})

Vue.filter('numberWithSpaces',function (x) {
    if (x === 0) {
        return 0
    }
    else if (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
})
