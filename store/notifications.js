const state = () => ({
    error: null,
    warning: null,
    troubleshoot: null
});

const getters = {
    getError: state => state.error,
    getWarning: state => state.warning,
    getTroubleshoot: state => state.troubleshoot
};

const mutations = {
    setError(state, error = null) {
        state.error = error;
    },
    setWarning(state, warning = null) {
        state.warning = warning;
    },
    setTroubleshooting(state, troubleshoot = null) {
        state.troubleshoot = troubleshoot;
    }
};

const actions = {
    setError: ({ commit }, error = null) => {
        commit('setError', error);
    },
    setWarning: ({ commit }, warning = null) => {
        commit('setWarning', warning);
    },
    setTroubleshooting: ({ commit }, warning = null) => {
        commit('setTroubleshooting', warning);
    },
    handle({commit}, res) {
        if(res.hasOwnProperty('sts') && res.rs.ty === 'TroubleshootWarning') {
            commit('setTroubleshooting', res.rs)
        }
        else if(res.hasOwnProperty('sts') && res.sts === 'warning') {
            let warning = {};
            warning.type = res.rs.ty
            warning.msg = res.rs.rsns[0]
            commit('setWarning', warning)
            commit('setError', warning)
        }
        else if(res.hasOwnProperty('sts') && res.sts === 'error') {
            let warning = {};
            warning.type = res.rs.ty
            console.log(res.rs);
            console.log(res.rs.rsns);
            warning.msg = res.rs.rsns[0]
            commit('setWarning', warning)
            commit('setError', warning)
        }
        return res;
    },
};

export default {
    state,
    getters,
    actions,
    mutations
};
