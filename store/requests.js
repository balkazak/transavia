export const actions = {
  async getFares({ commit }, data) {
    try {
      return await this.$axios.$post('fares', data);
    } catch (e) {
      console.log(e)
    }
  }
}
