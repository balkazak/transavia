module.exports = {
    title: 'Search ticket',
    show: 'Show',
    hide: 'Hide',
    there: 'There',
    back: 'Back',
    close: 'Close',
    till: 'Till',
    passenger: 'Passenger',
    passengers: 'Passengers',
    buyer: 'Buyer',
    refundable: 'Refundable',
    non_refundable: 'Non-refundable',
    another_time: 'Another time',
    cheapest: 'The cheapest',
    fastest: 'The fastest',
    direct_flight: 'Forward flight',
    direct_flight_stop: 'Forward flight with stops',
    baggage: 'Baggage',
    create_ticket: 'Create ticket',
    buy_till: 'Buy till',
    departure: 'Departure',
    arrival: 'Arrival',
    duration: 'Duration',
    flights: 'Flights',
    No_return_ticket_required: 'No return ticket required',
    expired_time: 'Prices could change! Refresh your search to see current prices.',
    refresh: 'Refresh',
    several_carriers: 'Flight is provided by ',
    schedule_changed: 'Schedule Changed',
    price_changed: 'Price is changed',
    continue: 'continue',
    all_airports: 'All Airports',
    find_tickets: 'Find tickets',
    for: 'for',
    price_for_all: 'Price for all passengers',
    country_code: 'Country code',
    book: 'Book a flight',
    booked: 'Booked',
    login: 'LOGIN',
    field_required: 'This field is required',
    valid_email: 'Please enter a valid email address',
    support: '24/7 Support',
    my_cabinet: 'My cabinet',
    go_out: 'Log out',
    your_balance: 'Your balance',
    reservation: 'Reservation',
    airline: 'Airline',
    route: 'Route',
    date: 'Date',
    status: 'Status',
    time: 'Time',
    price: 'Price',
    detail: 'Detail',
    pay: 'Pay',
    download_ticket: 'Download ticket',
    tickets: 'Tickets',
    settings: 'Settings',
    commission_tickets: 'Commission on air tickets (UAH.)',
    change: 'Change',
    change_password: 'Change password',
    old_password: 'Old password',
    new_password: 'New password',
    repeat_new_password: 'Repeat new password',
    ticket_commission_changed: 'Ticket commission changed',
    order_paid: 'Your order must be paid before:',
    sent_reservation: 'We sent your reservation to your mail.',
    to_pay: 'To pay',
    airline_tariff: 'Airline tariff',
    taxes_fees: 'Taxes and fees',
    agency_fee: 'Agency fee',
    tr_fee: 'Transavia fee',
    total: 'Total',
    airport: 'Airport',
    cancel_succes: 'Your ticket successfully cancelled.',
    dropdowns: {
        passengers: 'Passengers amount',
        adult: 'Adult',
        children: 'Children',
        l_child: 'Children',
        age_1: 'from 2 to 12 y.o.',
        age_2: 'to 2 y.o.',
        class_e: 'Economy',
        class_b: 'Business',
        class_y: 'Youth',
        way_1: 'One way',
        way_2: 'Round trip'
    },
    widget: {
        from: 'From',
        to: 'To'
    },
    calendar: {
        price_graph: 'PRICE GRAPH',
        price_table: 'PRICE TABLE'
    },
    form: {
        name: 'Full name',
        surname: 'Surname',
        first_name: 'Name',
        gender: 'Gender',
        b_day: 'Birth date',
        citizenship: 'Citizenship',
        document_type: 'Document type',
        document_number: 'Document number',
        document_date: 'Expired date',
        phone: 'Phone',
        choose: 'Choose',
        day: 'DD',
        month: 'MM',
        year: 'YYYY',
        email: 'Email',
        phone_number: 'Phone number'
    },
    tariff: {
        title: 'Tariff terms',
        condition: 'Tariff Application Conditions',
        avia_tariffs: 'Airline tariffs',
        tariff_type_1: 'Budgetary',
        tariff_type_2: 'Optimal',
        tariff_type_3: 'Comfort',
    },
    details: {
        details_f: 'Flight details',
        company: 'Flight provided by company',
        rebook: 'Rebook with surcharge',
        route: 'Route',
        flight_company: 'F. company',
        flight: 'Flight',
        class: 'Class',
        plane: 'Plane',
        promo: '«Special promo» tariff',
        carry_baggage: 'Carry-on baggage',
        change_route: 'Change route',
        baggage_allowance: 'Baggage allowance up to'
    },
    texts: {
        send_mail: 'We will send an electronic ticket to this address',
        send_sms: 'For SMS notifications about booking status',
        your_name: 'How can we call you?',
        validation: 'To avoid difficulties boarding, write down the name and surname (s) exactly as they are written on your passport (ID).',
        new_search: 'Make a new search'
    }
}
