import Vue from 'vue'
import $ from 'jquery/dist/jquery.min'

if (process.browser) {
  window.$ = $;
}
import 'popper.js/dist/popper.min'
import 'vue-loaders/dist/vue-loaders.css'
import SuiVue from 'semantic-ui-vue'
import PortalVue from 'portal-vue';
//import 'bootstrap/dist/js/bootstrap.min'
import vuelidate from 'vuelidate'
import VueLoaders from 'vue-loaders'
import bFormSlider from 'vue-bootstrap-slider';
import 'bootstrap-slider/dist/css/bootstrap-slider.css'

Vue.use(bFormSlider)
Vue.use(VueLoaders);
//Vue.use(datePicker);
Vue.use(SuiVue);
Vue.use(PortalVue);
Vue.use(vuelidate);
// Vue.use(GoTop);
Vue.use(require('vue-moment'));
