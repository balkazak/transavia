//import i18n from 'static/locales';

const state = () => ({
  lang: 'en'
});

const mutations = {
  ru(state) {
    state.lang = 'ru';
  },
  en(state) {
    state.lang = 'en';
  }
};

const actions = {};

const getters = {
  get_locale: (state) => state.lang
};

export default {
  state,
  getters,
  actions,
  mutations
};
